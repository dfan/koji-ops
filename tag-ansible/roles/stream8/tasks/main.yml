---

# Externalrepos

- name: Create an external repo for module builds
  koji_external_repo:
    name: "c8s-build-repo"
    url: "https://kojihub.stream.rdu2.redhat.com/kojifiles/repos/c8s-build/latest/$arch/"
    state: present

- name: Create an external repo of signing tools 
  koji_external_repo:
    name: "c8s-signing"
    url: "http://infra-mirror.stream.rdu2.redhat.com/signing/el8/$arch/"


# RPM tags

- name: Create the toplevel tag
  koji_tag:
    name: "c8s"
    packages:
      centos-stream: "{{ allowlist_pkgs }}"

- name: Create the buildtools tag
  koji_tag:
    name: c8s-build-tools
    groups:
      build:
        - bash
        - bzip2
        - centos-stream-release
        - centpkg-minimal
        - coreutils
        - cpio
        - diffutils
        - findutils
        - gawk
        - gcc
        - gcc-c++
        - grep
        - gzip
        - info
        - make
        - patch
        - redhat-rpm-config
        - rpm-build
        - scl-utils-build
        - sed
        - shadow-utils
        - tar
        - unzip
        - util-linux
        - which
        - xz
      srpm-build:
        - bash
        - centos-stream-release
        - centpkg-minimal
        - gnupg2
        - redhat-rpm-config
        - rpm-build
        - scl-utils-build
        - shadow-utils
    inheritance:
      - parent: "c8s"
        priority: 10

- name: Create the signing buildtools tag
  koji_tag:
    name: c8s-signing-build-tools
    groups:
      build:
        - bash
        - bzip2
        - centos-stream-release
        - centpkg-minimal
        - coreutils
        - cpio
        - diffutils
        - findutils
        - gawk
        - glibc-minimal-langpack
        - grep
        - gzip
        - info
        - make
        - patch
        - redhat-rpm-config
        - rh-dupsign-macros
        - rpm-build
        - sed
        - shadow-utils
        - tar
        - unzip
        - util-linux
        - which
        - xz
      prep-kerberos:
        - krb5-workstation
        - rh-pesign-macros
        - rh-signing-tools-lite
        - rpm-python
      srpm-build:
        - bash
        - centos-stream-release
        - centpkg-minimal
        - glibc-minimal-langpack
        - gnupg2
        - redhat-rpm-config
        - rpm-build
        - shadow-utils
    packages: 
      centos-stream: 
        - rh-dupsign-macros

- name: Create the released tag
  koji_tag:
    name: "c8s-released"
    inheritance:
      - parent: "c8s-pending"
        priority: 10

- name: Create the pending tag
  koji_tag:
    name: "c8s-pending"
    inheritance:
      - parent: "c8s"
        priority: 10

- name: Create the pending-signed tag
  koji_tag:
    name: "c8s-pending-signed"
    inheritance:
      - parent: "c8s"
        priority: 10

- name: Create the gate tag
  koji_tag:
    name: "c8s-gate"
    inheritance:
      - parent: "c8s"
        priority: 10

- name: Create the candidate tag
  koji_tag:
    name: "c8s-candidate"
    inheritance:
      - parent: "c8s"
        priority: 10

- name: Create the override tag
  koji_tag:
    name: "c8s-override"
    inheritance:
      - parent: "c8s"
        priority: 10

- name: Create the build tag
  koji_tag:
    name: "c8s-build"
    arches: "{{ arches }}"
    extra:
      mock.new_chroot: 0
      mock.package_manager: dnf
      mock.yum.module_hotfixes: 1
      repo_include_all: True
      rhpkg_dist: el8
      rpm.macro.dist: '.el8'
    inheritance:
      - parent: "c8s-build-tools"
        priority: 2
      - parent: "c8s-override"
        priority: 3
      - parent: "c8s-pending"
        priority: 5
      - parent: "buildroot-modules-c8s"
        priority: 10
      - parent: "java-openjdk-portable-result" # https://issues.redhat.com/browse/CS-2013
        priority: 17
# Build targets

- name: create the candidate target
  koji_target:
    name: "c8s-candidate"
    build_tag: "c8s-build"
    dest_tag: "c8s-gate"


# Signing/Compose tags
- name: Create the compose tag
  koji_tag:
    name: "c8s-compose"
    inheritance:
      - parent: "c8s-override"
        priority: 3
      - parent: "c8s-pending-signed"
        priority: 5

# Module tags
- name: Create a module build tag
  koji_tag:
    name: "module-c8s-build"
    inheritance:
      - parent: "buildroot-modules-c8s"
        priority: 10
    arches: "{{ arches }}"  
    external_repos:
      - repo: "c8s-build-repo"
        priority: 0

- name: Create a module build tag
  koji_tag:
    name: "buildroot-modules-c8s"
    inheritance:
      - parent: "module-javapackages-tools-201801-8050020210907194719-79ce6533"
        priority: 100
      - parent: "module-gimp-2.8-8000020190628145146-4148dfdf"
        priority: 120
      - parent: "module-virt-rhel-820231214001737-9edba152"
        priority: 140
      - parent: "module-go-toolset-rhel8-820231031173453-b754926a"
        priority: 150
      - parent: "module-httpd-2.4-820230817140043-9edba152"
        priority: 160
      - parent: "module-idm-client-820231122122737-49cc9d1b"
        priority: 180
      - parent: "module-llvm-toolset-rhel8-820240105093033-9edba152"
        priority: 200
      - parent: "module-mariadb-10.3-8030020210419150013-30b713e6"
        priority: 220
      - parent: "module-nodejs-10-8030020210304194401-30b713e6"
        priority: 240
      - parent: "module-postgresql-10-8060020211217182223-d63f516d"
        priority: 260
      - parent: "module-perl-App-cpanminus-1.7044-8060020220407150132-a439c6c3"
        priority: 300
      - parent: "module-perl-DBD-MySQL-4.046-8060020220407150447-4f86f5e0"
        priority: 320
      - parent: "module-perl-YAML-1.24-8060020220407151730-f7485d8d"
        priority: 340
      - parent: "module-php-7.2-8020020200507003613-2c7ca891"
        priority: 360
      - parent: "module-pki-deps-10.6-820240322145646-9edba152"
        priority: 380
      - parent: "module-ruby-2.5-8050020220221230938-b4937e53"
        priority: 480
      - parent: "module-rust-toolset-rhel8-820240119204620-b09eea91"
        priority: 500
      - parent: "module-subversion-1.10-8070020220512143541-78111232"
        priority: 520
      - parent: "module-swig-3.0-8030020201104011322-30b713e6"
        priority: 540
      - parent: "module-jmc-rhel8-8050020211110222101-6392b1f8"
        priority: 580
      - parent: "module-container-tools-rhel8-820231016065943-20125149"
        priority: 620
      - parent: "module-python36-3.6-820231207164005-17efdbc7"
        priority: 640
      - parent: "module-jaxb-4-820230420121335-7dadbc74"
        priority: 660

        

- name: Create a modules tag
  koji_tag:
    name: "el8-modules"
    packages:
      centos-stream: "{{ allowlist_modules }}"


- name: Create a modules candidate tag
  koji_tag:
    name: "el8-modules-candidate"
    inheritance:
      - parent: "el8-modules"
        priority: 10

- name: Create a modules override tag
  koji_tag:
    name: "el8-modules-override"
    inheritance:
      - parent: "el8-modules-candidate"
        priority: 10

- name: Create a modules dev compose tag
  koji_tag:
    name: "el8-modules-dev-compose"
    inheritance:
      - parent: "el8-modules-candidate"
        priority: 10

- name: Create a modules build tag
  koji_tag:
    name: "el8-modules-build"
    inheritance:
      - parent: "el8-modules-override"
        priority: 10

- name: Create a modules gate tag
  koji_tag:
    name: "el8-modules-gate"
    inheritance:
      - parent: "el8-modules"
        priority: 10

- name: Create a modules pending tag
  koji_tag:
    name: "el8-modules-pending"
    inheritance:
      - parent: "el8-modules"
        priority: 10

- name: Create a modules pending signed tag
  koji_tag:
    name: "el8-modules-pending-signed"
    inheritance:
      - parent: "el8-modules"
        priority: 10

# Image tags/targets

- name: 'make an image base tag'
  koji_tag:
    name: "guest-c8s"
    packages:
      centos-stream: "{{ image_allowlist_pkgs }}"

- name: 'make an image candidate tag'
  koji_tag:
    name: 'guest-c8s-candidate'
    inheritance:
      - parent: 'guest-c8s'
        priority: 10

- name: 'make a container build tag'
  koji_tag:
    name: 'guest-c8s-container-build'
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
    inheritance:
      - parent: 'guest-c8s-candidate'
        priority: 10

- name: 'make an image build tag'
  koji_tag:
    name: 'guest-c8s-image-build'
    arches: "{{ arches }}"
    extra:
      mock.package_manager: dnf
    inheritance:
      - parent: 'guest-c8s-candidate'
        priority: 10

- name: 'make a container target'
  koji_target:
    name: "c8s-containers"
    build_tag: "guest-c8s-container-build"
    dest_tag: "guest-c8s-candidate"

- name: 'make an image target'
  koji_target:
    name: "c8s-images"
    build_tag: "guest-c8s-image-build"
    dest_tag: "guest-c8s-candidate"

# pesign
- name: 'make a pesign build tag'
  koji_tag:
    state: present
    name: "c8s-pesign-build"
    perm: admin
    arches: "{{ arches }}"
    extra:
      mock.new_chroot: 0
      mock.package_manager: dnf
      rhpkg_dist: el8
      rpm.macro.dist: '.el8'
    external_repos:
      - repo: 'c8s-signing'
        priority: 0
    inheritance:
      - parent: "c8s-signing-build-tools"
        priority: 2
      - parent: "c8s-build"
        priority: 5

- name: 'pesign inheritance'
  koji_tag_inheritance:
    parent_tag: "c8s-build"
    child_tag: "c8s-pesign-build"
    priority: 10

- name: pesign default target
  koji_target:
    name: "c8s-candidate-pesign"
    build_tag: "c8s-pesign-build"
    dest_tag: "c8s-gate"
    

# Long running sidetags
#
# Portable openjdk builds
# (also in other Stream releases)
# https://issues.redhat.com/browse/CS-1618
# https://issues.redhat.com/browse/CS-2013



- name: "make the java-openjdk-c8s-build tag"
  koji_tag:
    name: java-openjdk-c8s-build
    arches: "{{ arches }}"
    inheritance:
      - parent: java-openjdk-portable-result
        priority: 10
      - parent: c8s-build
        priority: 20



- name: "make the java-openjdk-c8s-build target"
  koji_target:
    name: java-openjdk-c8s-build
    build_tag: java-openjdk-c8s-build
    dest_tag: java-openjdk-c8s-build

- name: make the firefox esr gate tag
  koji_tag:
    state: present
    name: 'c8s-firefox-esr-102-stack-gate'
    inheritance:
      - parent: "c8s"
        priority: 10

- name: make the firefox esr build tags
  koji_tag:
    state: present
    name: 'c8s-firefox-esr-102-stack-build'
    arches: "{{ arches }}"
    inheritance:
      - parent: 'c8s-firefox-esr-102-stack-gate'
        priority: 2
      - parent: 'module-nodejs-12-8040020210202170320-9f9e2e7e'
        priority: 6
      - parent: 'c8s-build'
        priority: 8
    extra: 
      downstream_sidetag: 'rhel-8.9.0-firefox-esr-102-stack-build'

- name: make the firefox esr gate target
  koji_target:
    name: 'c8s-firefox-esr-102-stack-gate'
    build_tag: 'c8s-firefox-esr-102-stack-build'
    dest_tag: 'c8s-firefox-esr-102-stack-gate'

- name: make the thunderbird esr gate tag
  koji_tag:
    state: present
    name: 'c8s-thunderbird-esr-102-stack-gate'
    inheritance:
      - parent: "c8s"
        priority: 10

- name: make the thunderbird esr build tags
  koji_tag:
    state: present
    name: 'c8s-thunderbird-esr-102-stack-build'
    arches: "{{ arches }}"
    inheritance:
      - parent: 'c8s-thunderbird-esr-102-stack-gate'
        priority: 2
      - parent: 'module-nodejs-12-8040020210202170320-9f9e2e7e'
        priority: 6
      - parent: 'c8s-build'
        priority: 8
    extra: 
      downstream_sidetag: 'rhel-8.9.0-thunderbird-esr-102-stack-build'

- name: make the thunderbird esr gate target
  koji_target:
    name: 'c8s-thunderbird-esr-102-stack-gate'
    build_tag: 'c8s-thunderbird-esr-102-stack-build'
    dest_tag: 'c8s-thunderbird-esr-102-stack-gate'
